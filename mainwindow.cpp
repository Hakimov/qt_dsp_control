#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Настройка serialUSB
    serial = new QSerialPort(this);
    serial->setPortName(ui->comboBox_SelectCOM->currentText());
    serial->setBaudRate(QSerialPort::Baud115200);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    serial->open(QIODevice::ReadWrite);
    serial->write(0);
    serial->readAll();

    //--
    // настройка таймера
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(checkCommandChannel()));
    //timer->start(100);

    // Начальная настройка блока
    on_comboBoxDemodulationMode_currentIndexChanged(7); // J3E USB
    on_spinBoxFrequency_valueChanged(1525000);   //127 кГц
    on_comboBox_Attenuator_currentIndexChanged(0); //аттенюатор 0 дБ
    on_comboBox_SwRRU_SwARU_currentIndexChanged("РРУ");
    on_horizontalSlider_LineLevel_valueChanged(168);
    on_horizontalSlider_LevelTLF_valueChanged(128);
    SetAmplifier_ARU_RRU(100);  // РРУ 153

}

MainWindow::~MainWindow()
{
    delete ui;
    serial->close();
}

// Установка модификации РПУ
void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    QByteArray  setModificationRPU;
    setModificationRPU[0] = SET_ModificationRPU;
    setModificationRPU[1] = 0x00;
    setModificationRPU[2] = 0x00;
    setModificationRPU[3] = 0x00;

    switch (index) {
    case 0:
        setModificationRPU[4] = 0;
        break;
    case 1:
        setModificationRPU[4] = 1;
        break;
    case 2:
        setModificationRPU[4] = 2;
        break;
    case 3:
        setModificationRPU[4] = 3;
        break;
    default:
        break;
    }

    setModificationRPU[5] = CRCcalculate(setModificationRPU, setModificationRPU.size());
    qDebug() << "Send:" << setModificationRPU.toHex();
    serial->write(setModificationRPU);
    Error_Handler();
}

// Расчет контрольной суммы
char MainWindow::CRCcalculate(QByteArray &data, int dataSize)
{
    char CRC = 0;
    for (int i = 0; i <= dataSize; ++i)
    {
        CRC += data[i];
    }
    CRC ^= 0xFF;
    CRC += 1;
    qDebug() << "CRC:" << CRC;
    return CRC;
}

// Обработка полученных данных
void MainWindow::Error_Handler()
{
    QByteArray temp;
    temp = serial->readAll();
    //qDebug() << "Receive:" << temp.toHex() << "\n--\n";
    if(temp.size() == 0){
        qDebug() << "Блок не отвечает";
        QMessageBox::critical(this, "Ошибка!","Блок не отвечает");
    }
    if(temp[temp.size()] != CRCcalculate(temp, temp.size()))
    {
        qDebug() << "Код сообщения 0х02\n"
                    "Ошибка контрольной суммы!";
        QMessageBox::critical(this, "Ошибка!",
                              "Код сообщения 0х02\n"
                              "Ошибка контрольной суммы!");
        // Вывести ошибку и не продолжать работу
    } else {

        switch (temp[0]) {
        case _ERROR_NO:
            qDebug() << "Ошибок нет!";
            break;
        case _ERROR_CRC:
            qDebug() << "Код сообщения 0х02 \n"
                        "Ошибка контрольной суммы!";
            QMessageBox::critical(this, "Ошибка!",
                                  "Код сообщения 0х02 \n"
                                  "Ошибка контрольной суммы!");
            break;
        case _ERROR_COMMAND:
            qDebug() << "Код сообщения 0х03 \n"
                        "Недопустимая команда!";
            QMessageBox::critical(this, "Ошибка!",
                                  "Код сообщения 0х03 \n"
                                  "Недопустимая команда!");
            break;
        case _ERROR_VAL:
            qDebug() << "Код сообщения 0х04 \n"
                        "Недопустимое значение параметра блока ЦОС!";
            QMessageBox::critical(this, "Ошибка!",
                                  "Код сообщения 0х04 \n"
                                  "Недопустимое значение параметра блока ЦОС!");
            break;
        default:
            break;
        }
    }
    qDebug() << "Receive:" << temp.toHex() << "\n--\n";
}

// Изменение вида работы
void MainWindow::on_comboBoxDemodulationMode_currentIndexChanged(int index)
{
    QByteArray setDemodulationMode;
    setDemodulationMode[0] = SET_DemodulationMode;
    setDemodulationMode[1] = index;
    setDemodulationMode[2] = ui->comboBoxBand->currentIndex();
    setDemodulationMode[3] = ui->comboBoxShift->currentIndex();
    setDemodulationMode[4] = ui->comboBoxSymbolRate->currentIndex();
    setDemodulationMode[5] = CRCcalculate(setDemodulationMode, setDemodulationMode.size());

    serial->write(setDemodulationMode);
    qDebug() << "Send:" << setDemodulationMode.toHex();

    QByteArray temp;
    temp = serial->readAll();
    qDebug() << "Receive:" << temp.toHex();
}

// Установка частоты настройки изделия
void MainWindow::on_spinBoxFrequency_valueChanged(int arg1)
{
    QByteArray bytesFrequensy;
    bytesFrequensy[0] = SET_Frequency;
    bytesFrequensy[1] = (arg1 & 0xFF000000)>>24;
    bytesFrequensy[2] = (arg1 & 0x00FF0000)>>16;
    bytesFrequensy[3] = (arg1 & 0x0000FF00)>>8;
    bytesFrequensy[4] = (arg1 & 0x000000FF);
    bytesFrequensy[5] = CRCcalculate(bytesFrequensy, bytesFrequensy.size());
    serial->write(bytesFrequensy);
    qDebug() << "Send:" << bytesFrequensy.toHex();
    Error_Handler();
}

// Установка АРУ РРУ
void MainWindow::SetAmplifier_ARU_RRU(int value)
{
    QByteArray SET_RRU;
    if(ui->comboBox_SwRRU_SwARU->currentText() == "РРУ")
    {
        SET_RRU[0] = SET_ManualTuneAmplification;
        SET_RRU[1] = 0x00;
        SET_RRU[2] = 0x00;
        SET_RRU[3] = 0x00;
        SET_RRU[4] = 153 - value;
        SET_RRU[5] = CRCcalculate(SET_RRU, SET_RRU.size());
        serial->write(SET_RRU);
        qDebug() << "Send:" << SET_RRU.toHex();
    }
    if(ui->comboBox_SwRRU_SwARU->currentText() == "АРУ")
    {
        SET_RRU[0] = SET_AutoTuneAmplification;
        SET_RRU[1] = 0x00;
        SET_RRU[1] = 0x00;
        SET_RRU[2] = ui->comboBox_ARU_Down->currentIndex();
        SET_RRU[3] = ui->comboBox_ARU_Up->currentIndex();
        SET_RRU[4] = 153 - value;
        SET_RRU[5] = CRCcalculate(SET_RRU, SET_RRU.size());
        serial->write(SET_RRU);
        qDebug() << "Send:" << SET_RRU.toHex();
    }
    Error_Handler();
}

// Регулировка РРУ и АРУ при изменении слайдера
void MainWindow::on_verticalSlider_RRU_valueChanged(int value)
{
    SetAmplifier_ARU_RRU(value);
    ui->label_RRU->setText(QString("%1").arg(value));
}

// Изменение усиления при установке РРУ или АРУ
void MainWindow::on_comboBox_SwRRU_SwARU_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "РРУ")
    {
        ui->comboBox_ARU_Up->setEnabled(false);
        ui->comboBox_ARU_Down->setEnabled(false);
    }
    if (arg1 == "АРУ")
    {
        ui->comboBox_ARU_Up->setEnabled(true);
        ui->comboBox_ARU_Down->setEnabled(true);
    }
    SetAmplifier_ARU_RRU(ui->verticalSlider_RRU->value());
}

// Изменение усиления при изменении нарастания
void MainWindow::on_comboBox_ARU_Up_currentIndexChanged()
{
    SetAmplifier_ARU_RRU(ui->verticalSlider_RRU->value());
}

// Изменение усиления при изменении спада
void MainWindow::on_comboBox_ARU_Down_currentIndexChanged()
{
    SetAmplifier_ARU_RRU(ui->verticalSlider_RRU->value());
}

// Изменение значения аттенюатора
void MainWindow::on_comboBox_Attenuator_currentIndexChanged(int index)
{
    QByteArray SET_Attenuator;
    SET_Attenuator[0] = SET_AttenuatorCode;
    SET_Attenuator[1] = 0x00;
    SET_Attenuator[2] = 0x00;
    SET_Attenuator[3] = 0x00;
    if((index >= 0) && (index <= 10)){
        SET_Attenuator[4] = index;
    }
    if(index == 11){    // если индекс 11
        SET_Attenuator[4] = 15;    // то автоматическое управление
    }
    SET_Attenuator[5] = CRCcalculate(SET_Attenuator, SET_Attenuator.size());
    serial->write(SET_Attenuator);
    qDebug() << "Send:" << SET_Attenuator.toHex();
    Error_Handler();
}

// Регулировка уровня входа линии
void MainWindow::on_horizontalSlider_LineLevel_valueChanged(int value)
{
    ui->label_LineLevel->setText(QString("%1").arg(15.000 -(15.000 + 48.000)*(168-value)/(168.000)));
    qDebug() << 168 - value;
    QByteArray SET_LineLevel;
    SET_LineLevel[0] = SET_LevelOutSignalInLine;
    SET_LineLevel[1] = 0x00;
    SET_LineLevel[2] = 0x00;
    SET_LineLevel[3] = 0x00;
    SET_LineLevel[4] = 168 - value;
    SET_LineLevel[5] = CRCcalculate(SET_LineLevel, SET_LineLevel.size());
    qDebug() << "Send:" << SET_LineLevel.toHex();
    serial->write(SET_LineLevel);
    Error_Handler();
}

// Регулировка уровня ТЛФ
void MainWindow::on_horizontalSlider_LevelTLF_valueChanged(int value)
{

    ui->label_LevelTLF->setText(QString("%1").arg(0.000 - (0.000 + 63.000)*(168.000 - value)/(168.000)));
    qDebug() << 168 - value;
    QByteArray SET_TLF;
    SET_TLF[0] = SET_TLFLevel;
    SET_TLF[1] = 0x00;
    SET_TLF[2] = 0x00;
    SET_TLF[3] = 0x00;
    SET_TLF[4] = 168 - value;
    SET_TLF[5] = CRCcalculate(SET_TLF, SET_TLF.size());
    serial->write(SET_TLF);
    qDebug() << "Send:" << SET_TLF.toHex();
    Error_Handler();
}


void MainWindow::on_pushButton_INIT_COM_clicked()
{

}

// Функция проверки командного канала, вызывается через таймер
void MainWindow::checkCommandChannel()
{
    ui->checkBox_KK->setEnabled(false);

    QByteArray Response_KK;
    Response_KK[0] = Response_CommandChannel;
    Response_KK[1] = 0x00;
    Response_KK[2] = 0x00;
    Response_KK[3] = 0x00;
    Response_KK[4] = 0x00;
    Response_KK[5] = CRCcalculate(Response_KK, Response_KK.size());
    serial->write(Response_KK);
    qDebug() << "Send:" << Response_KK.toHex();

    QByteArray temp;
    temp = serial->readAll();
    qDebug() << "Send:" << temp.toHex();

    if(temp[temp.size()] != CRCcalculate(temp, temp.size()))
    {
        qDebug() << "CRC ERROR"
                    "Код сообщения 0х02\n"
                    "Ошибка контрольной суммы!";
        QMessageBox::critical(this, "Ошибка!",
                              "Код сообщения 0х02\n"
                              "Ошибка контрольной суммы!");
        // Вывести ошибку и не продолжать работу
    }
/*
    if (temp[1] == 1)
    {
        ui->checkBox_KK->setChecked(false);
        ui->checkBox_KK->toggled(false);
    }

    if (temp[1] == 0)
    {
        ui->checkBox_KK->setChecked(true);
        ui->checkBox_KK->toggled(true);
    }
*/
}

void MainWindow::on_close_triggered()
{
    close();
}

void MainWindow::on_about_triggered()
{
    QMessageBox::information(this, "О программе!",
                          "Программа для дистанционного управления блоком ЦОС Б9-406 изделия Р-170ПМЦ. "
                          "Взаимодействие с блоком производится по интерфейсу UART в соответсвии с протоколом "
                          "информационно-технического сопряжения блока.\n\n"
                          "");
}

void MainWindow::on_about_b9_406_triggered()
{
    QMessageBox::information(this, "Б9-406",
                          "Блок ЦОС Б9-406 обеспечивает двухсторонний обмен информацией с блоком управления "
                          "Б10-387 по интерфейсу UART с сигналами LVDS......."     );
}


void MainWindow::on_dialTone_valueChanged(int value)
{
    ui->dialTone->notchesVisible();
    ui->label_Tone->setText(QString("%1").arg(value * 10));
    qDebug() << ui->dialTone->value();
    QByteArray SET_Tone;
    SET_Tone[0] = SET_FrequencyTone;
    SET_Tone[1] = 0x00;
    SET_Tone[2] = 0x00;
    SET_Tone[3] = (value & 0xFF00)>>8;
    SET_Tone[4] = (value & 0x00FF);
    SET_Tone[5] = CRCcalculate(SET_Tone, SET_Tone.size());
    serial->write(SET_Tone);
    qDebug() << "Send:" << SET_Tone.toHex();
    Error_Handler();
}

// Самоконтроль блока (почему то нет ответа от блока)
void MainWindow::on_pushButton_SelfControl_clicked()
{
    /*
    Перед выполнением самоконтроля настроить блок на:
    - режим демодуляции J3E USB;
    - частоту настройки 127 кГц (не забыть включить опору 129,6 МГц);
    - аттенюатор 0 дБ;
    - автоматический режим регулировки усиления с постоянными времени 0.01/0.01 с.
    */
    on_comboBoxDemodulationMode_currentIndexChanged(7); // J3E USB
    on_spinBoxFrequency_valueChanged(127000);   //127 кГц
    on_comboBox_Attenuator_currentIndexChanged(0); //аттенюатор 0 дБ
    ui->comboBox_ARU_Down->setCurrentIndex(0);  // АРУ 0,01 с
    ui->comboBox_ARU_Up->setCurrentIndex(0);  // АРУ 0,01 с
    SetAmplifier_ARU_RRU(ui->verticalSlider_RRU->maximum());  // АРУ 153

    // Можно приступать к самоконтролю
    QByteArray SET_SelfControl;
    SET_SelfControl[0] = SelfControl;
    SET_SelfControl[1] = 0x00;
    SET_SelfControl[2] = 0x00;
    SET_SelfControl[3] = 0x00;
    SET_SelfControl[4] = 0x00;
    SET_SelfControl[5] = CRCcalculate(SET_SelfControl, SET_SelfControl.size());
    serial->write(SET_SelfControl);
    qDebug() << "Send_SelfControl:" << SET_SelfControl.toHex();

    QByteArray temp;
    temp = serial->readAll();
    qDebug() << "Receive_SelfControl:" << temp.toHex() << "\n--\n";

    //Error_Handler();

}

// Выбор COM
void MainWindow::on_comboBox_SelectCOM_currentIndexChanged(const QString &arg1)
{
    serial->close();
    // Настройка serialUSB
    serial = new QSerialPort(this);
    serial->setPortName(arg1);
    serial->setBaudRate(QSerialPort::Baud115200);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    serial->open(QIODevice::ReadWrite);
    serial->write(0);
    serial->readAll();
}


