#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QtSerialPort/QSerialPort>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_comboBoxDemodulationMode_currentIndexChanged(int index);

    void on_spinBoxFrequency_valueChanged(int arg1);

    void on_verticalSlider_RRU_valueChanged(int value);

    void on_comboBox_SwRRU_SwARU_currentIndexChanged(const QString &arg1);

    void on_comboBox_ARU_Up_currentIndexChanged();

    void on_comboBox_ARU_Down_currentIndexChanged();

    void on_comboBox_Attenuator_currentIndexChanged(int index);

    void on_horizontalSlider_LineLevel_valueChanged(int value);

    void on_horizontalSlider_LevelTLF_valueChanged(int value);

    void on_pushButton_INIT_COM_clicked();

    void on_close_triggered();

    void on_about_triggered();

    void on_about_b9_406_triggered();

    void on_dialTone_valueChanged(int value);

    void on_pushButton_SelfControl_clicked();

    void on_comboBox_SelectCOM_currentIndexChanged(const QString &arg1);

    void on_comboBox_currentIndexChanged(int index);

public slots:
    void checkCommandChannel();


private:
    Ui::MainWindow *ui;
    QTimer *timer;
    QSerialPort *serial;
    char CRCcalculate(QByteArray &data, int dataSize);
    void Error_Handler();
    void SetAmplifier_ARU_RRU(int value);
};

enum DSPErrMessage
{
    //Возможные значения:
    _ERROR_NO = 0x01,       //0х01 – нет ошибок,
    _ERROR_CRC = 0x02,      //0х02 – ошибка контрольной суммы,
    _ERROR_COMMAND = 0x03,  //0х03 – недопустимая команда,
    _ERROR_VAL = 0x04       //0x04 – недопустимое значение параметра блока ЦОС
};

enum DSPDemodulationMode
{
    DemodulationMode_0_A1A = 0,
    DemodulationMode_1_A2A = 1,
    DemodulationMode_2_H2A = 2,
    DemodulationMode_3_J2A = 3,
    DemodulationMode_4_A3E = 4,
    DemodulationMode_5_R3E = 5,
    DemodulationMode_6_H3E = 6,
    DemodulationMode_7_J3E_USB = 7,
    DemodulationMode_8_J3E_LSB = 8,
    DemodulationMode_9_B8E = 9,
    DemodulationMode_10_F3EJ = 10,
    DemodulationMode_11_F3EA = 11,
    DemodulationMode_12_F1B = 12,
    DemodulationMode_13_G1B = 13,
    DemodulationMode_14_F7B = 14
};

enum CommandCode
{
    SET_ModificationRPU = 0x01,  // Установка модификации РПУ
    SET_PCHInput = 0x02,  // Переключение входа ПЧ на передней панели
    SET_APD = 0x03,  // Переключение АПД
    SET_Frequency = 0x04,  // Установка частоты настройки
    Response_CommandChannel = 0x05,  // Запрос наличия командного канала
    SET_FrequencyShift = 0x06,  // Установка сдвига частоты несущей принимаемого сигнала относительно частоты настройки
    SET_DemodulationMode = 0x07, // Установка вида работы
    SET_FrequencyTone = 0x08,   // Установка частоты тона
    SET_LevelOutSignalInLine = 0x09, // Установка уровня громкости сигнала для телефонного выхода
    SET_TLFLevel = 0x0A, // Установка уровня громкости сигнала для телефонного выхода
    SET_AttenuatorCode = 0x0C,  // Управление аттенюатором
    Response_AttenuatorCode = 0x0D, // Запрос текущего кода ослабления аттенюатора
    SET_ManualTuneAmplification = 0x0E,     // Установка ручного режима регулировки усиления
    SET_AutoTuneAmplification = 0x0F,   // Установка автоматического режима регулировки усиления
    Reset_AutoTuneAmplification = 0x10,    //Сброс автоматической регулировки усиления
    Response_CurrentAmplificationCode = 0x11, //Запрос текущего кода усиления
    Response_OutSignalLevel_InLine = 0x12, // Запрос уровня выходного сигнала в линии
    Response_SignalLevelAntennaInput = 0x13,    // Запрос уровня сигнала на антенном входе
    SET_AdditionMode = 0x14, // Включение/выключение режима сложения
    SET_Regenerator_OnOff = 0x15, // Включение/выключение регенератора
    SET_PolarityTLG = 0x16, // Установка полярности телеграфного выхода
    Response_ChannelQuality = 0x17,  //Запрос качества (ТЛГ искажений) канала
    Response_APD = 0x21, // Запрос состояния АПД
    Response_ParametersChannelSettings = 0x18, // Запрос параметров настройки канала приема одной командой
    Response_BitFrequecyCompare = 0x19, // Запрос сигнала биений при сверке частоты ОГ РПУ
    SelfControl = 0x20  // Самоконтроль
};


#endif // MAINWINDOW_H
